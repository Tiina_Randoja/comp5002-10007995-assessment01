﻿using System;

namespace comp5002_10007995_assessment01
{
    class Program
    {
        static void Main(string[] args)
        { 
        //Start the program with Clear();
        Console.Clear();
        
    
        
        //Welcome message
        Console.WriteLine("*********************************");
        Console.WriteLine("******* Welcome to TiMart *******");
        Console.WriteLine("*********************************");
        Console.WriteLine("");
        
        //Asking for the user to insert their name, storing it as a variable ("name")
        Console.WriteLine("Please insert your name");
        var name = Console.ReadLine();
        Console.WriteLine("");

        //Displaying a greeting using the "name"    
        Console.WriteLine($"Hello {name}, always a pleasure to see you!");
        Console.WriteLine("");
        
        //Asking the user to insert the first price and storing it as variable ("price1")        
        Console.WriteLine("Please insert the first price, with two decimal places / for example : 12.00");
        var price1 = double.Parse(Console.ReadLine());
        Console.WriteLine("");
        
        //Displaying a message stating the amount that was inserted
        Console.WriteLine($"{price1:C2} has been added");
        Console.WriteLine("");
        
        //Question: Add another price? Number <1> for adding another or <2> for finishing and paying            
            Console.WriteLine("Would you like to add another item?");
            Console.WriteLine("");
            Console.WriteLine("Press <1> to add the item or <2> to finish and pay");
            Console.WriteLine("");
            
        //Storing the answer as variable "answer"            
            var answer = int.Parse(Console.ReadLine());

        //Based on the answer, allowing two options  
        //Option 1: If decision to add another item
            
            if (answer == 1)
           
                {
                //Asking the user to insert the second price using two decimal places
                Console.WriteLine("Please insert the second price, also with two decimal places");
                Console.WriteLine("");
                
                //Storing it a variable ("price2")            
                var price2 = double.Parse(Console.ReadLine());
                
                //Displaying a message stating the amount that was added
                Console.WriteLine($"{price2:C2} has been added");
                Console.WriteLine("");

                //Calculating the total between the two prices that were added and store it in a variable ("total")       
                var total = (price1+price2);
                
                //Calculating 15% of the "total" and storing it as "GST"
                var GST = (total/100*15);
                Console.WriteLine($"The total to be paid (including GST) is {total+GST:C2}");
                 }
       
        //Option 2: If decision to pay now   
                  
                      if (answer == 2)
           
                     {     
                     //Calculating 15% of the "price1" and storing it as "GST"    
                     var GST = (price1/100*15);

                     //Calculate and display the the total to be paid by adding the GST("GST") to the inserted price ("price1")
                     Console.WriteLine($"The total to be paid (including GST) is {price1+GST:C2}");
                      }
         
         //Displaying an option to finish and pay
         Console.WriteLine("");
         Console.WriteLine("Press <Enter> to finish and pay");
         Console.ReadKey();
         Console.WriteLine("");
         
         // Payment Received
         //Displaying a goodbye message using the user’s name ("name")         
         Console.WriteLine($"Thank you {name} for shopping at TiMart");
         Console.WriteLine("");
         Console.WriteLine("*****************");
         Console.WriteLine("Have a great day!");
         Console.WriteLine("*****************");
         
                     Console.WriteLine("");
                     Console.WriteLine("");
        
        //Allowing the user to press any key to close the program
        Console.WriteLine("Press any key to close the program");
        Console.ReadKey();
         
        }
    }
}
